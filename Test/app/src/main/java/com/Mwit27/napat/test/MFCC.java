package com.Mwit27.napat.test;

import java.io.File;
import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.AudioProcessor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import be.tarsos.dsp.util.fft.FFT;
import be.tarsos.dsp.util.fft.HammingWindow;
import be.tarsos.dsp.io.android.AudioDispatcherFactory;
import be.tarsos.dsp.AudioDispatcher;
import android.content.Context;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import  android.app.Activity;
import android.app.Activity;
import java.io.InputStream;
import java.io.FileInputStream;
import java.util.Arrays;

import be.tarsos.dsp.io.TarsosDSPAudioFormat;
//import javax.sound.sampled.AudioInputStream;
import be.tarsos.dsp.io.UniversalAudioInputStream;

public class MFCC implements AudioProcessor {

    private int amountOfCepstrumCoef; //Number of MFCCs per frame
    protected int amountOfMelFilters; //Number of mel filters (SPHINX-III uses 40)
    protected float lowerFilterFreq; //lower limit of filter (or 64 Hz?)
    protected float upperFilterFreq; //upper limit of filter (or half of sampling freq.?)

    float[] audioFloatBuffer;
    //Er zijn evenveel mfccs als er frames zijn!?
    //Per frame zijn er dan CEPSTRA coëficienten
    private float[] mfcc;
    float[] mfvalue;
    int centerFrequencies[];
    private FFT fft;
    private int samplesPerFrame;
    private float sampleRate;

    public MFCC(int samplesPerFrame, int sampleRate) {
        this(samplesPerFrame, sampleRate, 30, 30, 133.3334f, ((float) sampleRate) / 2f);
    }

    public MFCC(int samplesPerFrame, float sampleRate, int amountOfCepstrumCoef, int amountOfMelFilters, float lowerFilterFreq, float upperFilterFreq) {
        this.samplesPerFrame = samplesPerFrame;
        this.sampleRate = sampleRate;
        this.amountOfCepstrumCoef = amountOfCepstrumCoef;
        this.amountOfMelFilters = amountOfMelFilters;
        this.fft = new FFT(samplesPerFrame, new HammingWindow());

        this.lowerFilterFreq = Math.max(lowerFilterFreq, 25);
        this.upperFilterFreq = Math.min(upperFilterFreq, sampleRate / 2);
        calculateFilterBanks();
    }

    @Override
    public boolean process(AudioEvent audioEvent) {
        audioFloatBuffer = audioEvent.getFloatBuffer().clone();

        // Magnitude Spectrum
        float bin[] = magnitudeSpectrum(audioFloatBuffer);
        // get Mel Filterbank
        float fbank[] = melFilter(bin, centerFrequencies);
        // Non-linear transformation
        float f[] = nonLinearTransformation(fbank);
        // Cepstral coefficients
        mfcc = cepCoefficients(f);

        return true;
    }

    @Override
    public void processingFinished() {

    }


    public float[] magnitudeSpectrum(float frame[]) {
        float magSpectrum[] = new float[frame.length];

        // calculate FFT for current frame

        fft.forwardTransform(frame);

        // calculate magnitude spectrum
        for (int k = 0; k < frame.length / 2; k++) {
            magSpectrum[frame.length / 2 + k] = fft.modulus(frame, frame.length / 2 - 1 - k);
            magSpectrum[frame.length / 2 - 1 - k] = magSpectrum[frame.length / 2 + k];
        }

        return magSpectrum;
    }

    public final void calculateFilterBanks() {
        centerFrequencies = new int[amountOfMelFilters + 2];

        centerFrequencies[0] = Math.round(lowerFilterFreq / sampleRate * samplesPerFrame);
        centerFrequencies[centerFrequencies.length - 1] = (int) (samplesPerFrame / 2);

        double mel[] = new double[2];
        mel[0] = freqToMel(lowerFilterFreq);
        mel[1] = freqToMel(upperFilterFreq);

        float factor = (float) ((mel[1] - mel[0]) / (amountOfMelFilters + 1));
        //Calculates te centerfrequencies.
        for (int i = 1; i <= amountOfMelFilters; i++) {
            float fc = (inverseMel(mel[0] + factor * i) / sampleRate) * samplesPerFrame;
            centerFrequencies[i - 1] = Math.round(fc);
        }

    }


    public float[] nonLinearTransformation(float fbank[]) {
        float f[] = new float[fbank.length];
        final float FLOOR = -50;

        for (int i = 0; i < fbank.length; i++) {
            f[i] = (float) Math.log(fbank[i]);

            // check if ln() returns a value less than the floor
            if (f[i] < FLOOR) f[i] = FLOOR;
        }

        return f;
    }


    public float[] melFilter(float bin[], int centerFrequencies[]) {
        float temp[] = new float[amountOfMelFilters + 2];

        for (int k = 1; k <= amountOfMelFilters; k++) {
            float num1 = 0, num2 = 0;

            float den = (centerFrequencies[k] - centerFrequencies[k - 1] + 1);

            for (int i = centerFrequencies[k - 1]; i <= centerFrequencies[k]; i++) {
                num1 += bin[i] * (i - centerFrequencies[k - 1] + 1);
            }
            num1 /= den;

            den = (centerFrequencies[k + 1] - centerFrequencies[k] + 1);

            for (int i = centerFrequencies[k] + 1; i <= centerFrequencies[k + 1]; i++) {
                num2 += bin[i] * (1 - ((i - centerFrequencies[k]) / den));
            }

            temp[k] = num1 + num2;
        }

        float fbank[] = new float[amountOfMelFilters];

        for (int i = 0; i < amountOfMelFilters; i++) {
            fbank[i] = temp[i + 1];
        }

        return fbank;
    }


    public float[] cepCoefficients(float f[]) {
        float cepc[] = new float[amountOfCepstrumCoef];

        for (int i = 0; i < cepc.length; i++) {
            for (int j = 0; j < f.length; j++) {
                cepc[i] += f[j] * Math.cos(Math.PI * i / f.length * (j + 0.5));
            }
        }

        return cepc;
    }

//    private static float centerFreq(int i,float samplingRate){
//        double mel[] = new double[2];
//        mel[0] = freqToMel(lowerFilterFreq);
//        mel[1] = freqToMel(samplingRate / 2);
//
//        // take inverse mel of:
//        double temp = mel[0] + ((mel[1] - mel[0]) / (amountOfMelFilters + 1)) * i;
//        return inverseMel(temp);
//    }

    protected static float freqToMel(float freq) {
        return (float) (2595 * log10(1 + freq / 700));
    }


    private static float inverseMel(double x) {
        return (float) (700 * (Math.pow(10, x / 2595) - 1));
    }


    protected static float log10(float value) {
        return (float) (Math.log(value) / Math.log(10));
    }

    public float[] getMFCC() {
        return mfcc.clone();
    }

    public int[] getCenterFrequencies() {
        return centerFrequencies;
    }

    public String getmfvalue() {
        Log.d("value2", Arrays.toString(mfvalue));
        return Arrays.toString(mfvalue);
    }

    public double[] gmval() {
        final double[] doubleArray = new double[mfvalue.length];
        for (int i = 0; i < mfvalue.length; i++) {
            doubleArray[i] = mfvalue[i];  // no casting needed
        }
        return doubleArray;
    }

    public String onMFCC(String path) {
        //   TextView viewmt1 = (TextView) context.findViewById(R.id.mt1);
        int sampleRate = 44100;
        int bufferSize = 1024;
        int bufferOverlap = 128;
        //final AudioDispatcher dispatcher = AudioDispacherFactory.fromDefaultMicrophone(22050,1024,512);
        //String path = getExternalCacheDir().getAbsolutePath() + "/saytest.mp4";
        // new AndroidFFMPEGLocator(this);
        InputStream inStream = null;
        try {
            inStream = new FileInputStream(path);

        } catch (FileNotFoundException ex) {
            Log.d("Error", "pathError");
            Log.d("Erro1", path);
            // insert code to run when exception occurs
        }
        AudioDispatcher dispatcher = new AudioDispatcher(new UniversalAudioInputStream(inStream, new TarsosDSPAudioFormat(sampleRate, bufferSize, 1, true, true)), bufferSize, bufferOverlap);
        final MFCC mfcc = new MFCC(bufferSize, sampleRate, 40, 50, 300, 3000);

        dispatcher.addAudioProcessor(mfcc);

        dispatcher.addAudioProcessor(new AudioProcessor() {

            @Override
            public void processingFinished() {
                //       textView.setText("Finish");
            }

            // @Override
            public boolean process(AudioEvent audioEvent) {

                mfcc.process(audioEvent);
                final float audio_float[] = mfcc.getMFCC();
                mfvalue = audio_float;
                Log.d("value", Arrays.toString(mfvalue));

                return true;
            }
        });
        new Thread(dispatcher, "Audio MFCC").start();
        Log.d("value", Arrays.toString(mfvalue));
        return Arrays.toString(mfvalue);
    }
}